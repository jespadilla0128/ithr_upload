<?php 
//set status for upload

Class Upload {
    public $status = 1;

    public function fileVerification($fullpath) {
        // check file type
        $ext = array('csv','json', 'yaml');
        $fileType = pathinfo($fullpath, PATHINFO_EXTENSION);
        if (!in_array($fileType, $ext)) {
            $this->$status = 0;
        }

        return;
    }

    public function startUpload($fname) {
        // Get file details and set file type verification
        $dir =  "files/";
        $filepath = $dir . basename($fname);

        // Check if $status is set to 0 by an error
        if ($this->status == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $filepath)) {
                echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
                $this->status = 0;
            }
        }
        
        $header = array();
        // parse uploaded file
        if($this->status == 1 ) {
           
            $h = fopen($filepath, "r");
            $container = array();
            while(($data = fgetcsv($h, 1000, ",")) !== false) {
                // echo details
                $container[] = $data;
            }
            fclose($h);
            $header = $container[0];
        
            session_start();
        
            $_SESSION['headers'] = $header;
            $_SESSION['data'] = $container;
            $_SESSION['filepath'] = $filepath;
            $_SESSION['filename'] = $filename;
            header("Location: http://localhost/ithr/view.php");
        }
  

    }

    public function checkFileExist($fpath){
        // Check if file already exists, remove or rename file by adding date and time to make it unique.
        if (file_exists($fpath)) {
            echo "Sorry, file already exists.";
            $this->$status = 0;
        }
    }
}

$filename = $_FILES['fileToUpload']['name'];
$dir =  "files/";
$fullpath = $dir . basename($filename);
$upload = new Upload();

$upload->fileVerification($fullpath);
$upload->checkFileExist($fullpath);
$upload->startUpload($fullpath);




