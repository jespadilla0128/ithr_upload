<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <header>
        <title> Show Table </title>
        <link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="./css/main.css">
        <link href="https://unpkg.com/bootstrap-table@1.17.1/dist/bootstrap-table.min.css" rel="stylesheet">
        <link href="https://unpkg.com/bootstrap-table@1.17.1/dist/extensions/fixed-columns/bootstrap-table-fixed-columns.min.css" rel="stylesheet">


    </header>
    <body>
        <a class="btn btn-lm btn-primary" href="<?php echo $_SESSION['filepath'] ?>" download="<?php echo $_SESSION['filename'] ?>"> Export </a> 
        <div name="display_div"  class="display_div table-wrapper-scroll-y">
            <table id="view_table" class="table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <!-- change this one through container array index 0 -->
                        <?php 
                            foreach ($_SESSION['headers'] as $head => $val) {
                                echo "<th> " . $val . " </th>";
                            }
                            echo "<th> Action </th>";
                        ?>
                    </tr>
                    <?php 
                        foreach ($_SESSION['data'] as $key => $data) {
                            if($key != 0 ) {
                                echo "<tr>";
                                foreach ($data as $row_data) {
                                    echo "<td>" . $row_data . "</td>";
                                }
                                echo "<td><input type='button' id='editBtn' class='btn btn-sm btn-success' value='Edit' data-value='" .$key . "' ></td>";
                                echo "<td><input type='button' id='delBtn' class='btn btn-sm btn-danger' value='Delete' data-value='" .$key . "' ></td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                </thead>
            </table>
        </div>
        <script type="text/javascript" src="./js/jquery-3.5.1.js"></script>
        <script type="text/javascript" src="./bootstrap/js/bootstrap.js"></script>
        <script src="./js/main.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://unpkg.com/bootstrap-table@1.17.1/dist/bootstrap-table.min.js"></script>
        <script src="https://unpkg.com/bootstrap-table@1.17.1/dist/extensions/fixed-columns/bootstrap-table-fixed-columns.min.js"></script>
    </body>
</html>