<?php 
declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class TestCode extends TestCase
{
    /** @test */
    public function fileVerification()
    {
        // test file write for data display and include some download
        $ext = array('csv','json', 'yaml');
        $fileType = pathinfo("data.csv", PATHINFO_EXTENSION);
        if (!in_array($fileType, $ext)) {
            return false;
        }
        return true;
    }
}