$(document).ready(function(){
    $("#delBtn").on("click",function(){
        deleteRecord();
    })
});

function deleteRecord(){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this record!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          swal("Record Deleted!", {
            icon: "success",
          });
        } else {
          swal("Something went wrong!");
        }
      });
}